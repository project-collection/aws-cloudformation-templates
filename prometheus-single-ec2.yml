---
AWSTemplateFormatVersion: 2010-09-09
Metadata:
  'AWS::CloudFormation::Interface':
    ParameterGroups:
      - Label:
          Default: 'Section 1: General tag information'
        Parameters:
          - ApplicationNameTag
          - EnvironmentTag
          - SubEnvironmentTag
          - ApplicationIDTag
          - CostReferenceTag
      - Label:
          Default: 'Section 2: Application information'
        Parameters:
          - ShortName
          - NodeNumber
          - EC2AvailabilityZone
          - EC2InstanceType
          - EC2AmiId
          - EC2KeyName
          - ECRRegistryId
          - KmsKeyId
          - EC2OSVolumeSize
          - EC2DataVolumeSize
          - S3EnvBucket
          - Backup
          - BackupRetention
      - Label:
          Default: 'Section 3: Control created resources'
        Parameters:
          - CreateEC2

Parameters:
  ApplicationNameTag:
    Type: String
    Description: Should be the Application short name. I.e. MyApp
    Default: iot

  EnvironmentTag:
    Type: String
    AllowedValues:
      - prd
      - iat
      - dev
    Default: 'dev'

  SubEnvironmentTag:
    Type: String
    Default: mon

  CostReferenceTag:
    Description: Enter a Cost Reference. If you are in a managed Environment always
      enter a PSP Element
    Type: String
    Default: H-450433-51-01

  ApplicationIDTag:
    Description: Enter the BeamID
    Type: String
    Default: '0'

  ShortName:
    Description: "Short name to distinguish possible multiple instance of the prometheus within same VPC"
    Type: String
    Default: 'prom'

  NodeNumber:
    Description: 'Enter a node number'
    Type: String
    Default: '1'

  EC2InstanceType:
    Description: 'EC2 instance type to use'
    Type: String
    Default: t2.medium
    AllowedValues:
      - t2.medium
      - t2.large
      - t2.xlarge
      - m4.large
      - m4.xlarge

  EC2AmiId:
    Description: 'AMI ID of Amazon Linux image.'
    Type: 'AWS::SSM::Parameter::Value<String>'
    Default: 'Default-Amazon-Linux-2-AMI-ID'

  EC2OSVolumeSize:
    Description: 'Size of the EBS used for the operating system in GiB.'
    Type: Number
    Default: 32
    MinValue: 8

  EC2AvailabilityZone:
    Description: 'Availability zone to use'
    Type: 'AWS::EC2::AvailabilityZone::Name'
    Default: eu-central-1a

  EC2DataVolumeSize:
    Description: 'Size of the EBS used for the application GiB.'
    Type: Number
    Default: 64
    MinValue: 8

  S3EnvBucket:
    Description: Bucket with configuration
    Type: String
    Default: 'env.repository.iot.comp.db.de-eu-central-1'

  EC2KeyName:
    Type: AWS::EC2::KeyPair::KeyName
    Description: "The name of the key pair that you must use to log in to the instance securely."
    Default: "iotcloudNonProd"

  KmsKeyId:
    Type: String
    Default: "arn:aws:kms:eu-central-1:704835361756:key/5b4e82c5-b64f-4448-8578-9eeae9c8f6db"

  ECRRegistryId:
    Type: String
    Default: '271727004210'
    AllowedValues:
      - '271727004210'
      - '704835361756'

  Backup:
    Description: 'Backup the system?'
    Type: String
    Default: 'Yes'
    AllowedValues:
      - 'Yes'
      - 'No'

  BackupRetention:
    Description: 'How many daily backups should be kept?'
    Type: Number
    Default: 7
    MinValue: 1
    MaxValue: 21

  CreateEC2:
    Description: >-
      Toggle EC2 creation
    Type: String
    Default: 'true'
    AllowedValues:
      - 'true'
      - 'false'

Mappings:
  AvailabilityZoneMap:
    eu-central-1a:
      ImportSubnetId: VPC1-AZ1Subnet1
    eu-central-1b:
      ImportSubnetId: VPC1-AZ2Subnet1
    eu-central-1c:
      ImportSubnetId: VPC1-AZ3Subnet1

Conditions:
  WithEC2: !Equals 
    - !Ref CreateEC2
    - 'true'
  InProdAccount: !Equals
    - !Ref AWS::AccountId
    - '271727004210'

Resources:

  ENISecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: !Sub 'Group for Prometheus ${EnvironmentTag}-${SubEnvironmentTag} infrastructure.'
      VpcId: !ImportValue VPC1-VPC-ID
      SecurityGroupIngress:
        - Description: SSH
          CidrIp: '10.0.0.0/8'
          FromPort: 22
          ToPort: 22
          IpProtocol: tcp
        - Description: Monitoring - prometheus
          CidrIp: '10.104.80.0/20'
          FromPort: 1
          ToPort: 65535
          IpProtocol: tcp
        - Description: ICMP to check reachability by network stack
          CidrIp: '0.0.0.0/0'
          FromPort: -1
          ToPort: -1
          IpProtocol: icmp

  ENI:
    Type: AWS::EC2::NetworkInterface
    Properties:
      Description: !Sub 'Interface for Prometheus ${EnvironmentTag}-${SubEnvironmentTag} server'
      GroupSet:
        - !GetAtt ENISecurityGroup.GroupId
      SourceDestCheck: true
      SubnetId:
        Fn::ImportValue:
          Fn::FindInMap: [AvailabilityZoneMap, !Ref EC2AvailabilityZone, ImportSubnetId]
      Tags:
        - Key: Name
          Value: !Sub 'Prometheus ${EnvironmentTag}-${SubEnvironmentTag}'
        - Key: Environment
          Value: !Ref EnvironmentTag

  SplunkSecurityGroupIngressRule:
    Condition: InProdAccount
    Type: AWS::EC2::SecurityGroupIngress
    Properties:
      Description: 'Allow Prometheus to access splunk indexers'
      GroupId:
        Fn::ImportValue:
          !Sub "iotcloud-SecGroup-Splunk-${EnvironmentTag}"
      SourceSecurityGroupId: !GetAtt ENISecurityGroup.GroupId
      IpProtocol: tcp
      FromPort: 8089
      ToPort: 8089

  EC2InstanceRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: !Sub '${ApplicationNameTag}-${EnvironmentTag}-${SubEnvironmentTag}-${ShortName}-ec2-instance-role'
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
        - Effect: Allow
          Principal:
            Service:
            - ec2.amazonaws.com
          Action:
          - 'sts:AssumeRole'
      Policies:
      - PolicyName: !Sub ${ApplicationNameTag}-${EnvironmentTag}-${SubEnvironmentTag}-${ShortName}-policy
        PolicyDocument:
          Version: '2012-10-17'
          Statement:
          - Effect: Allow
            Action:
            - kms:Decrypt
            Resource: "*"
          - Effect: Allow
            Action:
            - tag:getResources
            Resource: "*"
          - Effect: Allow
            Action:
            - ssm:DescribeAssociation
            - ssm:GetDeployablePatchSnapshotForInstance
            - ssm:GetDocument
            - ssm:GetManifest
            - ssm:GetParameter*
            - ssm:ListAssociations
            - ssm:ListInstanceAssociations
            - ssm:PutInventory
            - ssm:PutComplianceItems
            - ssm:PutConfigurePackageResult
            - ssm:UpdateAssociationStatus
            - ssm:UpdateInstanceAssociationStatus
            - ssm:UpdateInstanceInformation
            Resource: "*"
          - Effect: Allow
            Action:
            - ec2messages:AcknowledgeMessage
            - ec2messages:DeleteMessage
            - ec2messages:FailMessage
            - ec2messages:GetEndpoint
            - ec2messages:GetMessages
            - ec2messages:SendReply
            Resource: "*"
          - Effect: Allow
            Action:
            - cloudwatch:PutMetricData
            - cloudwatch:ListMetrics
            - cloudwatch:GetMetricStatistics
            Resource: "*"
          - Effect: Allow
            Action:
            - ec2:DescribeInstanceStatus
            - ec2:DescribeInstances
            - ec2:DescribeTags
          - Effect: Allow
            Action:
            - ecs:ListClusters
            Resource: "*"
          - Effect: Allow
            Action:
            - ecr:GetAuthorizationToken
            - ecr:GetDownloadUrlForLayer
            - ecr:BatchGetImage
            - ecr:BatchCheckLayerAvailability
            Resource: "*"
          - Effect: Allow
            Action:
            - logs:CreateLogGroup
            - logs:CreateLogStream
            - logs:DescribeLogGroups
            - logs:DescribeLogStreams
            - logs:PutLogEvents
            Resource: "*"
          - Effect: Allow
            Action:
            - 's3:GetObject'
            - 's3:GetObjectVersion'
            Resource: !Sub 'arn:aws:s3:::${S3EnvBucket}/${EnvironmentTag}/trusted-cert-auth/*'
          - Effect: Allow
            Action:
            - 's3:GetObject'
            - 's3:GetObjectVersion'
            Resource: !Sub 'arn:aws:s3:::${S3EnvBucket}/${EnvironmentTag}/mon/alerta/*'
          - Effect: Allow
            Action:
            - 's3:GetObject'
            - 's3:GetObjectVersion'
            Resource: !Sub 'arn:aws:s3:::${S3EnvBucket}/${EnvironmentTag}/mon/httpd/*'
          - Effect: Allow
            Action:
            - 's3:ListBucket'
            Resource: !Sub 'arn:aws:s3:::${S3EnvBucket}'


  EC2InstanceProfile:
    Type: AWS::IAM::InstanceProfile
    Properties:
      Roles:
      - !Ref EC2InstanceRole
      InstanceProfileName: !Sub '${ApplicationNameTag}-${EnvironmentTag}-${SubEnvironmentTag}-${ShortName}-ec2-instance-profile'

  EC2DataVolume:
    Type: AWS::EC2::Volume
    Properties:
      AvailabilityZone: !Ref EC2AvailabilityZone
      Encrypted: true
      KmsKeyId: !Ref KmsKeyId
      Size: !Ref EC2DataVolumeSize
      VolumeType: gp2
      Tags:
        - Key: Name
          Value: !Sub '${ApplicationNameTag}-${EnvironmentTag}-${SubEnvironmentTag}-${ShortName}-ec2-data-volume'
        - Key: Environment
          Value: !Ref EnvironmentTag
        - Key: prom_node
          Value: !Ref NodeNumber

  EC2HostInstance:
    Type: AWS::EC2::Instance
    Condition: WithEC2
    Properties:
      AvailabilityZone: !Ref EC2AvailabilityZone
      BlockDeviceMappings:
        - DeviceName: /dev/xvda
          Ebs:
            DeleteOnTermination: true
            VolumeSize: !Ref EC2OSVolumeSize
            VolumeType: gp2
      ImageId: !Ref EC2AmiId
      InstanceInitiatedShutdownBehavior: stop
      InstanceType: !Ref EC2InstanceType
      CreditSpecification:
        CPUCredits: standard
      KeyName: !Ref EC2KeyName
      NetworkInterfaces:
        - DeviceIndex: 0
          NetworkInterfaceId: !Ref ENI
      Volumes:
        - VolumeId: !Ref EC2DataVolume
          Device: /dev/xvdc
      Tags:
        - Key: Name
          Value: !Sub '${ApplicationNameTag}-${EnvironmentTag}-${SubEnvironmentTag}-${ShortName}-ec2-instance'
        - Key: Environment
          Value: !Ref EnvironmentTag
        - Key: Backup
          Value: !Ref Backup
        - Key: BackupRetention
          Value: !Ref BackupRetention
        - Key: prom_node
          Value: !Ref NodeNumber
      IamInstanceProfile: !Ref EC2InstanceProfile
      UserData:
        "Fn::Base64":
          !Sub |
            #!/bin/bash -ex
            /opt/aws/bin/cfn-init -v --stack ${AWS::StackName} --resource EC2HostInstance --configsets initEnvironment,updateEnvironment --region ${AWS::Region}
    Metadata:
      AWS::CloudFormation::Init:
        configSets:
          initEnvironment:
            - "setup-cfn-hup"
            - "first-launch"
          updateEnvironment:
            - "first-launch"
        setup-cfn-hup:
           files:
             '/etc/cfn/cfn-hup.conf':
               content: !Sub |
                 [main]
                 stack=${AWS::StackId}
                 region=${AWS::Region}
                 interval=1
               mode: '000400'
               owner: root
               group: root
             '/etc/cfn/hooks.d/cfn-auto-reloader.conf':
               content: !Sub |
                 [cfn-auto-reloader-hook]
                 triggers=post.update
                 path=Resources.EcsInstanceLaunchConfiguration.Metadata.AWS::CloudFormation::Init
                 action=/opt/aws/bin/cfn-init --verbose --stack=${AWS::StackName} --region=${AWS::Region} --resource=EcsInstanceLaunchConfiguration --configsets updateEnvironment
                 runas=root
               mode: '000400'
               owner: root
               group: root
           services:
             sysvinit:
               cfn-hup:
                 enabled: true
                 ensureRunning: true
                 files:
                 - '/etc/cfn/cfn-hup.conf'
                 - '/etc/cfn/hooks.d/cfn-auto-reloader.conf'
        first-launch:
          commands:
            00-Initial-Run:
              cwd: "/root/scripts"
              command: "/root/scripts/initial-run.sh"
          files:
            "/root/scripts/initial-run.sh":
              content: !Sub |
                #!/bin/bash

                GITLAB_URI="git.tech.rz.db.de"
                GITLAB_PROJECT_PATH=$(aws ssm get-parameter --region ${AWS::Region} --name /${EnvironmentTag}/mon/prometheus/gitlab_project_path --query "Parameter.Value" --with-decryption --output text)
                GITLAB_ACCESS_USER=$(aws ssm get-parameter --region ${AWS::Region} --name /${EnvironmentTag}/mon/prometheus/gitlab_access_user --query "Parameter.Value" --with-decryption --output text)
                GITLAB_ACCESS_TOKEN=$(aws ssm get-parameter --region ${AWS::Region} --name /${EnvironmentTag}/mon/prometheus/gitlab_access_token --query "Parameter.Value" --with-decryption --output text)

                rm -rf /root/git
                git clone "https://$GITLAB_ACCESS_USER:$GITLAB_ACCESS_TOKEN@$GITLAB_URI/$GITLAB_PROJECT_PATH" --branch ${EnvironmentTag} --single-branch /root/git

                rsync -r /root/git/scripts/ /root/scripts/
                find /root/scripts -type f -exec chmod +rx {} \;

                (exec "/root/scripts/configure-proxy.sh")
                (exec "/root/scripts/update-and-install.sh")
                (exec "/root/scripts/configure-certs.sh")
                (exec "/root/scripts/configure-postfix.sh")
                (exec "/root/scripts/init-prometheus-data-volume.sh")
                (exec "/root/scripts/install-docker-compose.sh")
                (exec "/root/scripts/fetch-config.sh")
                (exec "/root/scripts/init-docker-compose.sh")

Outputs:
  InstanceId:
    Condition: WithEC2
    Description: EC2 instance id
    Value: !Ref EC2HostInstance
    Export:
      Name: !Sub '${ApplicationNameTag}-${EnvironmentTag}-${SubEnvironmentTag}-${ShortName}-ec2-instance'

  VolumeId:
    Description: EBS volume id for application data
    Value: !Ref EC2DataVolume
