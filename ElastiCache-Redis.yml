AWSTemplateFormatVersion: 2010-09-09
Metadata:
  'AWS::CloudFormation::Interface':
    ParameterGroups:
      - Label:
          default: General Tag Information
        Parameters:
          - ApplicationNameTag
          - EnvironmentTag
          - SubEnvironmentTag
          - InstanceTag
          - CostReferenceTag
          - ApplicationIDTag
      - Label:
          default: ElastiCache Configuration
        Parameters:
          - InstanceType
          - InstanceAZ
          - RedisAuthToken
          - ForeignInboundSecurityGroup
          - AdditionalForeignInboundSecurityGroup
          - SSMPathRedisAuthToken
          - SSMVersionRedisAuthToken
          - NumCacheClusters
          - AtRestEncryptionEnabled
          - TransitEncryptionEnabled
          - AutoMinorVersionUpgrade
          - AutomaticFailoverEnabled
Parameters:
  ApplicationNameTag:
    Type: String
    Description: Should be the Application short name. I.e. MyApp
    Default: iot
  EnvironmentTag:
    Description: What kind of environment is this?
    Type: String
    AllowedValues:
      - prd
      - iat
      - dev
    Default: prd
  SubEnvironmentTag:
    Description: normally this is main
    Type: String
    Default: main
  InstanceTag:
    Description: Instance name, e.g. pxr
    Type: String
    Default: pxr
  CostReferenceTag:
    Description: >-
      Enter a Cost Reference. If you are in a managed Environment always enter a
      PSP Element
    Type: String
    Default: H-450433-51-03
  ApplicationIDTag:
    Description: Enter the BeamID
    Type: String
    Default: '0'
  SSMPathRedisAuthToken:
    Description: Path to Redis Auth Token in SSM Parameter Store (without environment prefix)
    Type: String
    Default: '/apps/frontend/RedisAuthToken'
  SSMVersionRedisAuthToken:
    Description: Version of Redis Auth Token in SSM Parameter Store
    Type: String
    Default: '1'
  InstanceType:
    Description: Server EC2 instance type
    Type: String
    Default: cache.m3.medium
    AllowedValues:
      - cache.t2.micro
      - cache.t2.small
      - cache.m3.medium
      - cache.r4.large
  ForeignInboundSecurityGroup:
    Description: Foreign Security Group to allow inbound traffic from
    Type: String
    ConstraintDescription: must be a valid SecurityGroup.
  AdditionalForeignInboundSecurityGroup:
    Description: Another Foreign Security Group to allow inbound traffic from (optional)
    Type: String
    ConstraintDescription: must be a valid SecurityGroup.
  NumCacheClusters:
    Description: The number of cache clusters for replication group
    Type: String
    Default: '2'
    AllowedValues:
      - '1'
      - '2'
      - '3'
  AtRestEncryptionEnabled:
    Type: String
    Default: 'true'
    AllowedValues:
      - 'true'
      - 'false'
  TransitEncryptionEnabled:
    Type: String
    Default: 'true'
    AllowedValues:
      - 'true'
      - 'false'
  AutoMinorVersionUpgrade:
    Type: String
    Default: 'true'
    AllowedValues:
      - 'true'
      - 'false'
  AutomaticFailoverEnabled:
    Type: String
    Default: 'true'
    AllowedValues:
      - 'true'
      - 'false'
Conditions:
  AdditionalSecurityGroupIsSet:
    !Not [!Equals ["", !Ref AdditionalForeignInboundSecurityGroup]]
Resources:
  ECSecGroup:
    Type: 'AWS::EC2::SecurityGroup'
    Properties:
      VpcId: !ImportValue VPC1-VPC-ID
      GroupDescription: Access to Redis
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: '6379'
          ToPort: '6379'
          SourceSecurityGroupId: !Ref ForeignInboundSecurityGroup
        - !If
          - AdditionalSecurityGroupIsSet
          - IpProtocol: tcp
            FromPort: '6379'
            ToPort: '6379'
            SourceSecurityGroupId: !Ref AdditionalForeignInboundSecurityGroup
          - !Ref "AWS::NoValue"
        - IpProtocol: icmp
          FromPort: '-1'
          ToPort: '-1'
          CidrIp: 10.0.0.0/8
        - IpProtocol: icmp
          FromPort: '-1'
          ToPort: '-1'
          CidrIp: 172.16.0.0/12
      Tags:
        - Key: Name
          Value: !Join
            - '-'
            - - !Ref ApplicationNameTag
              - !Ref SubEnvironmentTag
              - !Ref EnvironmentTag
              - !Ref InstanceTag
              - Redis
        - Key: ApplicationName
          Value: !Ref ApplicationNameTag
        - Key: Environment
          Value: !Ref EnvironmentTag
        - Key: SubEnvironment
          Value: !Ref SubEnvironmentTag
        - Key: CostReference
          Value: !Ref CostReferenceTag
        - Key: ApplicationID
          Value: !Ref ApplicationIDTag
  ECSubnetGroup:
    Type: "AWS::ElastiCache::SubnetGroup"
    Properties:
      CacheSubnetGroupName: !Join
            - '-'
            - - !Ref ApplicationNameTag
              - !Ref SubEnvironmentTag
              - !Ref EnvironmentTag
              - !Ref InstanceTag
              - ECSubnetGroup
      Description: ElastiCache SubnetGroup
      SubnetIds:
        - !ImportValue VPC1-AZ1Subnet1
        - !ImportValue VPC1-AZ2Subnet1
        - !ImportValue VPC1-AZ3Subnet1
  ECRepGroup:
    Type: "AWS::ElastiCache::ReplicationGroup"
    Properties:
      AtRestEncryptionEnabled: !Ref AtRestEncryptionEnabled
      TransitEncryptionEnabled: !Ref TransitEncryptionEnabled
      AuthToken: !Sub '{{resolve:ssm-secure:/${EnvironmentTag}${SSMPathRedisAuthToken}:${SSMVersionRedisAuthToken}}}'
      AutoMinorVersionUpgrade: !Ref AutoMinorVersionUpgrade
      AutomaticFailoverEnabled: !Ref AutomaticFailoverEnabled
      CacheNodeType: !Ref InstanceType
      CacheSubnetGroupName: !Ref ECSubnetGroup
      Engine: redis
      EngineVersion: 3.2.6
      NumCacheClusters: !Ref NumCacheClusters
      Tags:
        - Key: Name
          Value: !Join
            - '-'
            - - !Ref ApplicationNameTag
              - !Ref SubEnvironmentTag
              - !Ref EnvironmentTag
              - !Ref InstanceTag
              - ECCluster
        - Key: ApplicationName
          Value: !Ref ApplicationNameTag
        - Key: Environment
          Value: !Ref EnvironmentTag
        - Key: SubEnvironment
          Value: !Ref SubEnvironmentTag
        - Key: CostReference
          Value: !Ref CostReferenceTag
        - Key: ApplicationID
          Value: !Ref ApplicationIDTag
      ReplicationGroupDescription: !Join
            - '-'
            - - !Ref ApplicationNameTag
              - !Ref SubEnvironmentTag
              - !Ref EnvironmentTag
              - !Ref InstanceTag
      ReplicationGroupId: !Join
            - '-'
            - - !Ref ApplicationNameTag
              - !Ref SubEnvironmentTag
              - !Ref EnvironmentTag
              - !Ref InstanceTag
      SecurityGroupIds:
        - !Ref ECSecGroup

